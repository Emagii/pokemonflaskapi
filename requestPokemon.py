import json
import requests

class pokemon(object):
    '''
    Creates a Pokemon object.
    Easier to get info about Pokemons with this??
    '''

    def __init__(self, id):
        '''
        Construction
        '''
        self.id = id
        self.req = requests.get('http://pokeapi.co/api/v2/pokemon/' + str(id) + '/')
        self.json_response = json.loads(self.req.content)

    def get_size(self):

        height = self.json_response['height']
        weight = str(self.json_response['weight'])
        return "Height: {0}\nWeight: {1}".format(height, weight)
    
    def get_png(self):
        '''
        Get the full set of sprites in a list
        '''
        pngs_list = []
        pngs = self.json_response['sprites']
        for value in pngs.items():
            pngs_list.append(value)
            
        return pngs.values()

    def get_name(self):
        name = self.json_response['name']
        return name
        
    
    def __str__(self):
    
        name = list(self.json_response['name'])
        name[0] = name[0].upper()
        return "".join(name)

def test(msg):
    return msg
