Install:
    
    Clone repo first


Create new virtualenvironment.

    python3 -m venv /path/to/new/virtual/

Windows:

    ./Scripts/activate.ps1
    
Linux:

    source ./Scripts/activate
    



Requirements

Package      Version
------------ ----------
certifi      2018.11.29
chardet      3.0.4
Click        7.0
Flask        1.0.2
idna         2.8
itsdangerous 1.1.0
Jinja2       2.10
MarkupSafe   1.1.0
pip          10.0.1
requests     2.21.0
setuptools   39.0.1
urllib3      1.24.1
Werkzeug     0.14.1

Install Preq:

    pip install -r requirements.txt
    
    
Start app

    python PokeApp.py
Go to http://localhost:5000    
