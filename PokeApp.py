from flask import Flask, render_template
import requestPokemon as RP
app = Flask(__name__)

@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', name='Home')

@app.route("/about")
def about():
    return render_template('about.html', name='About')

@app.route('/pokemon/<int:pokemon_id>')
@app.route('/pokemon/<pokemon_id>')
def show_post(pokemon_id):
    P = RP.pokemon(pokemon_id)
    # show the post with the given id, the id is an integer
    #return 'Pokemon ID: %d' % pokemon_id
    return render_template('pokemon.html', pngs=P.get_png(), name=P, pokemon=P)

if __name__ == '__main__':
    app.run(debug=True)